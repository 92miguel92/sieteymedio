//
//  ViewController.swift
//  SieteYMedia
//
//  Created by Master Móviles on 24/10/2016.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var newgame = JuegoSieteyMedia()
    var dinero = 100
    var ganadas = 0
    var perdidas = 0
    var posicion = 0
    
    @IBOutlet weak var dineroLb: UILabel!
    @IBOutlet weak var apuestaText: UITextField!
    @IBOutlet weak var ganadasLb: UILabel!
    @IBOutlet weak var perdidasLb: UILabel!
    @IBOutlet weak var pedirBt: UIButton!
    @IBOutlet weak var surrenderBt: UIButton!
    @IBOutlet var newGameBt: UIButton!
    var cartasView = [UIImageView]()
    
    
    @IBAction func newGame(_ sender: UIButton) {
        
        newgame = JuegoSieteyMedia()
        
        if(!pedirBt.isEnabled){
            pedirBt.isEnabled = true
        }
        if(!surrenderBt.isEnabled){
            surrenderBt.isEnabled = true
        }
        /*if(self.view.subviews.count > 0){*/
        if(!cartasView.isEmpty){
            for i in 0..<cartasView.count{
                cartasView[i].removeFromSuperview()
            }
        }
        self.posicion = 0
    }
    
    @IBAction func askCard(_ sender: UIButton) {
        
        newgame.addCarta(mano: (newgame.mano))
        /*if(newGameBt.isEnabled){
            newGameBt.isEnabled = false
        }*/
        self.repartirCarta(carta: newgame.mano.carta.last! ,enPosicion: self.posicion)
        self.posicion += 1
        print(newgame.mano.carta.description)
    }
    
    @IBAction func surrender(_ sender: UIButton) {
        
        var puntos: Double
        var puntosR = 0.0
        
        newgame.plantarse()
        puntos = newgame.sumCartas(mano: (newgame.mano))
        print("Mi mano \(puntos)")
        pedirBt.isEnabled = false
        surrenderBt.isEnabled = false
        newGameBt.isEnabled = true
        
        if(newgame.plantar){
            
            while(newgame.sumCartas(mano: (newgame.manoRobot)) < 6.0){
                newgame.addCarta(mano: newgame.manoRobot)
            }
            
            puntosR = newgame.sumCartas(mano: (newgame.manoRobot))
            print("Mi robot \(puntosR)")
        }
        
        let alertPerdido = UIAlertController(
            title: "El robot ha sacado \(puntosR)",
            message: "¡¡Has perdido con \(puntos)!!",
            preferredStyle: UIAlertControllerStyle.alert)
        let actionPerdido = UIAlertAction(
            title: "Aceptar",
            style: UIAlertActionStyle.default)
        
        let alertGanado = UIAlertController(
            title: "El robot ha sacado \(puntosR)",
            message: "¡¡Has ganado con \(puntos)!!",
            preferredStyle: UIAlertControllerStyle.alert)
        let actionGanado = UIAlertAction(
            title: "Aceptar",
            style: UIAlertActionStyle.default)
        
        if(puntos <= 7.5){
            if(puntosR > 7.5){
                if(!(apuestaText.text?.isEmpty)!){
                    dinero += Int(apuestaText.text!)!
                }
                ganadas += 1
                alertGanado.addAction(actionGanado)
                self.present(alertGanado, animated: true, completion: nil)
                
            }else if(puntos > puntosR){
                if(!(apuestaText.text?.isEmpty)!){
                    dinero += Int(apuestaText.text!)!
                }
                ganadas += 1
                alertGanado.addAction(actionGanado)
                self.present(alertGanado, animated: true, completion: nil)
            }else{
                if(!(apuestaText.text?.isEmpty)!){
                    dinero -= Int(apuestaText.text!)!
                }
                perdidas += 1
                alertPerdido.addAction(actionPerdido)
                self.present(alertPerdido, animated: true, completion: nil)
            }
        }else{
            if(!(apuestaText.text?.isEmpty)!){
                dinero -= Int(apuestaText.text!)!
            }
            perdidas += 1
            alertPerdido.addAction(actionPerdido)
            self.present(alertPerdido, animated: true, completion: nil)
        }
        dineroLb.text = String(dinero)
        ganadasLb.text = String(ganadas)
        perdidasLb.text = String(perdidas)
    }

    func repartirCarta(carta: Carta, enPosicion: Int){
        let nombreImagen = String(carta.valor)+String(carta.palo.rawValue)
        let imagenCarta = UIImage(named: nombreImagen)
        let cartaView = UIImageView(image: imagenCarta)
        cartaView.frame = CGRect(x: -200, y: -200, width: 200, height: 300)
        cartasView.append(cartaView)
        cartaView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        self.view.addSubview(cartaView)
        UIView.animate(withDuration: 0.5){
            cartaView.frame = CGRect(x:100+70*(enPosicion-1), y:100, width:70, height:100)
            cartaView.transform = CGAffineTransform(rotationAngle: 0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: (0/255.0), green: (150/255.0), blue: (0/255.0), alpha: 1.0)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

enum Palo: String{
    case bastos, copas, espadas, oros
}

struct Carta{
    var valor: Int
    var palo: Palo
    
    init?(valor: Int, palo: Palo){
        
        if(valor > 12 || valor < 1) {
            return nil
        }
        self.valor = valor
        self.palo = palo
    }
    func descripcion() -> String{
        return "el \(valor) de \(palo)"
    }
}

class Mano {
    
    var carta = [Carta]()
    var baraja = Baraja()
    
    init(){
        carta = [Carta]()
    }
    
    func addCarta(){
        
        baraja.barajar()
        carta.append(baraja.repartirCarta())
    }
}

class Baraja {
    
    var cartas = [Carta]()
    
    init(){
        for i in 1...12{
            
            cartas.append(Carta(valor:i,palo:Palo.oros)!)
            cartas.append(Carta(valor:i,palo:Palo.espadas)!)
            cartas.append(Carta(valor:i,palo:Palo.copas)!)
            cartas.append(Carta(valor:i,palo:Palo.bastos)!)
        }
    }
    
    func repartirCarta() -> Carta{
        return cartas.popLast()!
    }
    
    func barajar(){
        self.cartas.shuffle()
    }
}

class JuegoSieteyMedia{
    
    var baraja: Baraja
    var mano: Mano
    var manoRobot: Mano
    var plantar: Bool
    
    init(){
        baraja = Baraja()
        baraja.barajar()
        mano = Mano()
        manoRobot = Mano()
        plantar = false
    }
    
    func sumCartas(mano: Mano)-> Double{
        
        var sum: Double = 0
        
        for carta in (mano.carta){
            
            if(carta.valor == 10 || carta.valor == 11 || carta.valor == 12){
                sum += 0.5
            }else{
                sum += Double(carta.valor)
            }
        }
        
        return sum
    }
    
    func addCarta(mano: Mano){
        mano.addCarta()
    }
    
    func plantarse(){
        self.plantar = true
    }
    
    /*func terminar(mano: Mano){
        self.sumCartas(mano: mano)
    }*/
}

extension Array {
    mutating func shuffle() {
        if count < 2 { return }
        for i in 0..<(count - 1) {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            if (i != j) {
                swap(&self[i], &self[j])
            }
        }
    }
}
