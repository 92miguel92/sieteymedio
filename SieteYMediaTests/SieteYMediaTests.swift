//
//  SieteYMediaTests.swift
//  SieteYMediaTests
//
//  Created by Master Móviles on 24/10/2016.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import XCTest
@testable import SieteYMedia

class SieteYMediaTests: XCTestCase {
    
    var mano : Mano?
    var baraja : Baraja?
    
    override func setUp() {
        super.setUp()
        
        baraja = Baraja()
        mano = Mano()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testConstrCarta() {
        
        let carta = Carta(valor: 3, palo: Palo.copas)
        let descripcion = "el 3 de copas"
        
        XCTAssertEqual(carta?.descripcion(), descripcion)
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testSacarCartaCount() {
        
        let expected = 47
        baraja?.repartirCarta()
        
        XCTAssertEqual(baraja?.cartas.count, expected)
    }
    
    func testManoCount() {
        
        let expected = 2
        
        mano?.addCarta()
        mano?.addCarta()
        
        XCTAssertEqual(mano?.carta.count, expected)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
